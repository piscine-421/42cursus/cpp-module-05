#include "Bureaucrat.hpp"
#include "Form.hpp"
#include <iostream>

int		main(void)
{
	Form	a38(75, 75, "A38");
	std::cout << "Created " << a38 << std::endl;
	Bureaucrat	obelix("Obelix", 150);
	std::cout << "Created " << obelix << std::endl;
	obelix.signForm(a38);
	Bureaucrat	asterix("Asterix", 1);
	std::cout << "Created " << asterix << std::endl;
	asterix.signForm(a38);
}
