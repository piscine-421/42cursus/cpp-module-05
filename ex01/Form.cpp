#include "Bureaucrat.hpp"
#include "Form.hpp"
#include <iostream>

void			Form::beSigned(Bureaucrat &bureaucrat)
{
	if (bureaucrat.getGrade() > grade_to_sign)
		throw (GradeTooLowException());
	else
		is_signed = true;
}

Form::~Form(void)
{
}

Form::Form(void): grade_to_execute(150), grade_to_sign(150), name("default")
{
	is_signed = false;
}

Form::Form(const Form &copy): grade_to_execute(copy.grade_to_execute), grade_to_sign(copy.grade_to_sign), name(copy.getName())
{
	is_signed = copy.is_signed;
}

Form::Form(int init_grade_to_execute, int init_grade_to_sign, std::string init_name): grade_to_execute(init_grade_to_execute), grade_to_sign(init_grade_to_sign), name(init_name)
{
	if (grade_to_execute < 1 || grade_to_sign < 1)
		throw (GradeTooHighException());
	else if (grade_to_execute > 150 || grade_to_sign > 150)
		throw (GradeTooLowException());
	is_signed = false;
}

int				Form::getGradeToExecute(void) const
{
	return (grade_to_execute);
}

int				Form::getGradeToSign(void) const
{
	return (grade_to_sign);
}

int				Form::getIsSigned(void) const
{
	return (is_signed);
}

std::string	Form::getName(void) const
{
	return (name);
}

char const		*Form::GradeTooHighException::what(void) const throw()
{
	return ("grade is too high");
}

char const		*Form::GradeTooLowException::what(void) const throw()
{
	return ("grade is too low");
}

Form			&Form::operator=(const Form &copy)
{
	this->is_signed = copy.is_signed;
	return (*this);
}

std::ostream	&operator<<(std::ostream &str, const Form &form)
{
	std::string	returned;

	returned = ", grade to execute " + std::to_string(form.getGradeToExecute()) + ", grade to sign " + std::to_string(form.getGradeToSign());
	if (form.getIsSigned())
		returned += ", signed";
	returned += ".";
	return (str << form.getName() << returned);
}
