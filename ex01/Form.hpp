#ifndef FORM_HPP
# define FORM_HPP
# include <exception>
# include <string>

class			Bureaucrat;

class			Form
{
	private:
		const int			grade_to_execute;
		const int			grade_to_sign;
		const std::string	name;
		bool				is_signed;
	public:
		void				beSigned(Bureaucrat &bureaucrat);
		~Form(void);
		Form(void);
		Form(int init_grade_to_execute, int init_grade_to_sign, std::string init_name);
		Form(const Form &copy);
		int					getGradeToExecute(void) const;
		int					getGradeToSign(void) const;
		int					getIsSigned(void) const;
		std::string			getName(void) const;
		class				GradeTooHighException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		class				GradeTooLowException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		Form				&operator=(const Form &copy);
};

std::ostream	&operator<<(std::ostream &str, const Form &form);

#endif
