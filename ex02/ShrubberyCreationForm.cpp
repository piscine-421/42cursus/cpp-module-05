#include "ShrubberyCreationForm.hpp"
#include <fstream>
#include <iostream>

ShrubberyCreationForm::~ShrubberyCreationForm(void)
{
}

ShrubberyCreationForm::ShrubberyCreationForm(void): AForm::AForm(137, 145, "ShrubberyCreationForm"), target("null")
{
}

ShrubberyCreationForm::ShrubberyCreationForm(const std::string &target_file): AForm::AForm(137, 145, "ShrubberyCreationForm"), target(target_file)
{
}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &copy): AForm::AForm(copy), target(copy.target)
{
}

ShrubberyCreationForm	&ShrubberyCreationForm::operator=(const ShrubberyCreationForm &copy)
{
	AForm::operator=(copy);
	this->target = copy.target;
	return (*this);
}

void					ShrubberyCreationForm::execute_form(void) const
{
	std::ofstream	new_file(target + "_shrubbery");

	new_file << "       _-_\n    /~~   ~~\\\n /~~         ~~\\\n{               }\n \\  _-     -_  /\n   ~  \\\\ //  ~\n_- -   | | _- _\n  _ -  | |   -_\n      // \\\\\n";
	new_file.close();
}

std::ostream			&operator<<(std::ostream &str, const ShrubberyCreationForm &form)
{
	std::string	returned;

	returned = ", grade to execute " + std::to_string(form.getGradeToExecute()) + ", grade to sign " + std::to_string(form.getGradeToSign()) + ", target " + form.get_target();
	if (form.getIsSigned())
		returned += ", signed";
	returned += ".";
	return (str << form.getName() << returned);
}

std::string				ShrubberyCreationForm::get_target(void) const
{
	return (target);
}
