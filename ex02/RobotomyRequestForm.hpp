#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP
# include "AForm.hpp"
# include <string>

class			Bureaucrat;

class			RobotomyRequestForm: public AForm
{
	private:
		std::string	target;
	public:
		void				execute_form(void) const;
		std::string				get_target(void) const;
		~RobotomyRequestForm(void);
		RobotomyRequestForm(void);
		RobotomyRequestForm(const std::string &target_file);
		RobotomyRequestForm(const RobotomyRequestForm &copy);
		RobotomyRequestForm	&operator=(const RobotomyRequestForm &copy);
};

std::ostream	&operator<<(std::ostream &str, const RobotomyRequestForm &form);

#endif
