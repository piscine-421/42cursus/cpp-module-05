#include "Bureaucrat.hpp"
#include "AForm.hpp"
#include <iostream>

Bureaucrat::~Bureaucrat(void)
{
}

Bureaucrat::Bureaucrat(void): name("default")
{
	grade = 150;
}

Bureaucrat::Bureaucrat(const Bureaucrat &copy): name(copy.getName())
{
	grade = copy.grade;
}

Bureaucrat::Bureaucrat(std::string init_name, int init_grade): name(init_name)
{
	if (init_grade < 1)
		throw (GradeTooHighException());
	else if (init_grade > 150)
		throw (GradeTooLowException());
	grade = init_grade;
}

int				Bureaucrat::getGrade(void) const
{
	return (grade);
}

std::string		Bureaucrat::getName(void) const
{
	return (name);
}

char const		*Bureaucrat::GradeTooHighException::what(void) const throw()
{
	return ("grade too high");
}

char const		*Bureaucrat::GradeTooLowException::what(void) const throw()
{
	return ("grade too low");
}

Bureaucrat		&Bureaucrat::operator++(int dummy)
{
	(void)dummy;
	if (grade - 1 < 1)
		throw (GradeTooHighException());
	grade--;
	return (*this);
}

Bureaucrat		&Bureaucrat::operator--(int dummy)
{
	(void)dummy;
	if (grade + 1 > 150)
		throw (GradeTooLowException());
	grade++;
	return (*this);
}

Bureaucrat		&Bureaucrat::operator=(const Bureaucrat &copy)
{
	grade = copy.grade;
	return (*this);
}

std::ostream	&operator<<(std::ostream &str, const Bureaucrat &bureaucrat)
{
	return (str << bureaucrat.getName() << ", bureaucrat grade " << bureaucrat.getGrade() << ".");
}

void			Bureaucrat::executeForm(const AForm &form)
{
	try
	{
		form.execute(*this);
		std::cout << name << " executed " << form.getName() << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << name << " couldn’t execute " << form.getName() << " because " << e.what() << std::endl;
	}
}

void			Bureaucrat::signForm(AForm &form)
{
	try
	{
		form.beSigned(*this);
		std::cout << name << " signed " << form.getName() << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << name << " couldn’t sign " << form.getName() << " because " << e.what() << std::endl;
	}
}
