#ifndef AFORM_HPP
# define AFORM_HPP
# include <exception>
# include <string>

class			Bureaucrat;

class			AForm
{
	private:
		const int			grade_to_execute;
		const int			grade_to_sign;
		const std::string	name;
		bool				is_signed;
	public:
		AForm(void);
		AForm(int init_grade_to_execute, int init_grade_to_sign, std::string init_name);
		AForm(const AForm &copy);
		void				beSigned(Bureaucrat &bureaucrat);
		void				execute(const Bureaucrat &executor) const;
		virtual void		execute_form(void) const = 0;
		virtual std::string	get_target(void) const = 0;
		virtual ~AForm(void);
		int					getGradeToExecute(void) const;
		int					getGradeToSign(void) const;
		int					getIsSigned(void) const;
		std::string			getName(void) const;
		class				FormNotSignedException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		class				GradeTooHighException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		class				GradeTooLowException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		AForm				&operator=(const AForm &copy);
};

std::ostream	&operator<<(std::ostream &str, const AForm &form);

#endif
