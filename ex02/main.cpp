#include "AForm.hpp"
#include "Bureaucrat.hpp"
#include <iostream>
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

int main(void)
{
	Bureaucrat				Jeremy("Jeremy", 1);
	std::cout << "Created " << Jeremy << std::endl;
	Bureaucrat				Kevin("Kevin", 150);
	std::cout << "Created " << Kevin << std::endl;
	std::cout << std::endl << "PresidentialPardonForm Test:" << std::endl;
	PresidentialPardonForm	PPForm("Pol Pot");
	std::cout << "Created " << PPForm << std::endl;
	Kevin.signForm(PPForm);
	Jeremy.signForm(PPForm);
	Kevin.executeForm(PPForm);
	Jeremy.executeForm(PPForm);
	std::cout << std::endl << "RobotomyRequestForm Test:" << std::endl;
	RobotomyRequestForm		a38("Astérix");
	std::cout << "Created " << a38 << std::endl;
	Kevin.signForm(a38);
	Jeremy.signForm(a38);
	Kevin.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	Jeremy.executeForm(a38);
	std::cout << std::endl << "ShrubberyCreationForm Test:" << std::endl;
	ShrubberyCreationForm	knight_form("Ni");
	std::cout << "Created " << knight_form << std::endl;
	Kevin.signForm(knight_form);
	Jeremy.signForm(knight_form);
	Kevin.executeForm(knight_form);
	Jeremy.executeForm(knight_form);
	return (0);
}
