#include "Bureaucrat.hpp"
#include <iostream>

int		main(void)
{
	std::cout << "Creating Bureaucrat Steven, grade 1" << std::endl;
	try
	{
		Bureaucrat	steven("Steven", 1);
		std::cout << "Output: " << steven << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Creating copy of Bureaucrat Steven" << std::endl;
	try
	{
		Bureaucrat	steven("Steven", 1);
		Bureaucrat	steven2(steven);
		std::cout << "Output: " << steven2 << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Incrementing Steven's grade from 1 (will throw exception)" << std::endl;
	try
	{
		Bureaucrat	steven("Steven", 1);
		steven++;
		std::cout << "Output: " << steven << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Decrementing Steven's grade from 1 to 2" << std::endl;
	try
	{
		Bureaucrat	steven("Steven", 1);
		steven--;
		std::cout << "Output: " << steven << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Creating default Bureaucrat" << std::endl;
	try
	{
		Bureaucrat	default_bureaucrat;
		std::cout << "Output: " << default_bureaucrat << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Decrementing default_bureaucrat's grade from 150 (will throw exception)" << std::endl;
	try
	{
		Bureaucrat	default_bureaucrat;
		default_bureaucrat--;
		std::cout << "Output: " << default_bureaucrat << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Incrementing default_bureaucrat's grade from 150 to 149" << std::endl;
	try
	{
		Bureaucrat	default_bureaucrat;
		default_bureaucrat++;
		std::cout << "Output: " << default_bureaucrat << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Creating Bureaucrat Mark with grade -56 (will throw exception)" << std::endl;
	try
	{
		Bureaucrat	mark("Mark", -56);
		std::cout << "Output: " << mark << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}

	std::cout << "Creating Bureaucrat Martin with grade 179 (will throw exception)" << std::endl;
	try
	{
		Bureaucrat	martin("Martin", 179);
		std::cout << "Output: " << martin << "\n" << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << "\n" << std::endl;
	}
}
