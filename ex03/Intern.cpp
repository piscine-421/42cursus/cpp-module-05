#include "AForm.hpp"
#include "Intern.hpp"
#include <iostream>

AForm	*Intern::makeForm(std::string name, std::string target)
{
	AForm	*new_form;

	new_form = AForm::makeForm(name, target);
	if (!new_form)
		std::cout << "No form of this name exists!" << std::endl;
	else
		std::cout << "Intern creates " << *new_form  << ", target " << new_form->get_target() << std::endl;
	return (new_form);
}

Intern::~Intern(void)
{
}

Intern::Intern(void)
{
}

Intern::Intern(const Intern &copy)
{
	(void)copy;
}

Intern	&Intern::operator=(const Intern &copy)
{
	(void)copy;
	return (*this);
}