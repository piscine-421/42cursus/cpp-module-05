#include "Bureaucrat.hpp"
#include "AForm.hpp"
#include <iostream>
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

void			AForm::beSigned(Bureaucrat &bureaucrat)
{
	if (bureaucrat.getGrade() > grade_to_sign)
		throw (GradeTooLowException());
	else
		is_signed = true;
}

void			AForm::execute(const Bureaucrat &executor) const
{
	if (!this->is_signed)
		throw (AForm::FormNotSignedException());
	else if (executor.getGrade() > this->grade_to_execute)
		throw (AForm::GradeTooLowException());
	else
		this->execute_form();
}

AForm			*AForm::makeForm(std::string name, std::string target)
{
	AForm	*form;

	form = NULL;
	form = PresidentialPardonForm::makeForm(form, name, target);
	form = RobotomyRequestForm::makeForm(form, name, target);
	form = ShrubberyCreationForm::makeForm(form, name, target);
	return (form);
}

AForm::~AForm(void)
{
}

AForm::AForm(void): grade_to_execute(150), grade_to_sign(150), name("default")
{
	is_signed = false;
}

AForm::AForm(const AForm &copy): grade_to_execute(copy.grade_to_execute), grade_to_sign(copy.grade_to_sign), name(copy.getName())
{
	is_signed = copy.is_signed;
}

AForm::AForm(int init_grade_to_execute, int init_grade_to_sign, std::string init_name): grade_to_execute(init_grade_to_execute), grade_to_sign(init_grade_to_sign), name(init_name)
{
	if (grade_to_execute < 1 || grade_to_sign < 1)
		throw (GradeTooHighException());
	else if (grade_to_execute > 150 || grade_to_sign > 150)
		throw (GradeTooLowException());
	is_signed = false;
}

int				AForm::getGradeToExecute(void) const
{
	return (grade_to_execute);
}

int				AForm::getGradeToSign(void) const
{
	return (grade_to_sign);
}

int				AForm::getIsSigned(void) const
{
	return (is_signed);
}

std::string	AForm::getName(void) const
{
	return (name);
}

char const		*AForm::FormNotSignedException::what(void) const throw()
{
	return ("form not signed");
}

char const		*AForm::GradeTooHighException::what(void) const throw()
{
	return ("grade is too high");
}

char const		*AForm::GradeTooLowException::what(void) const throw()
{
	return ("grade is too low");
}

AForm			&AForm::operator=(const AForm &copy)
{
	this->is_signed = copy.is_signed;
	return (*this);
}

std::ostream	&operator<<(std::ostream &str, const AForm &form)
{
	std::string	returned;

	returned = ", grade to execute " + std::to_string(form.getGradeToExecute()) + ", grade to sign " + std::to_string(form.getGradeToSign());
	if (form.getIsSigned())
		returned += ", signed";
	returned += ".";
	return (str << form.getName() << returned);
}
