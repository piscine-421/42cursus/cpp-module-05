#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP
# include <iostream>

class			AForm;

class			Bureaucrat
{
	private:
		int					grade;
		const std::string	name;
	public:
		~Bureaucrat(void);
		Bureaucrat(void);
		Bureaucrat(std::string init_name, int init_grade);
		Bureaucrat(const Bureaucrat &copy);
		int					getGrade(void) const;
		std::string			getName(void) const;
		class				GradeTooHighException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		class				GradeTooLowException: public std::exception
		{
			public:
				virtual char const	*what(void) const throw();
		};
		Bureaucrat			&operator++(int dummy);
		Bureaucrat			&operator--(int dummy);
		Bureaucrat			&operator=(const Bureaucrat &copy);
		void				executeForm(const AForm &form);
		void				signForm(AForm &form);
};

std::ostream	&operator<<(std::ostream &str, const Bureaucrat &bureaucrat);

#endif
