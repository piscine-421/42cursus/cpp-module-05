#include "AForm.hpp"
#include "Bureaucrat.hpp"
#include "PresidentialPardonForm.hpp"
#include <iostream>

AForm					*PresidentialPardonForm::makeForm(AForm *form, std::string name, std::string target)
{
	if (!form && name == "presidential pardon")
		return (new PresidentialPardonForm(target));
	return (form);
}

PresidentialPardonForm::~PresidentialPardonForm(void)
{
}

PresidentialPardonForm::PresidentialPardonForm(void): AForm::AForm(5, 25, "PresidentialPardonForm"), target("null")
{
}

PresidentialPardonForm::PresidentialPardonForm(const std::string &target_file): AForm::AForm(5, 25, "PresidentialPardonForm"), target(target_file)
{
}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &copy): AForm::AForm(copy), target(copy.target)
{
}

PresidentialPardonForm	&PresidentialPardonForm::operator=(const PresidentialPardonForm &copy)
{
	AForm::operator=(copy);
	this->target = copy.target;
	return (*this);
}

void					PresidentialPardonForm::execute_form(void) const
{
	std::cout << target << " has been pardoned by Zaphod Beeblebrox." << std::endl;
}

std::ostream			&operator<<(std::ostream &str, const PresidentialPardonForm &form)
{
	std::string	returned;

	returned = ", grade to execute " + std::to_string(form.getGradeToExecute()) + ", grade to sign " + std::to_string(form.getGradeToSign()) + ", target " + form.get_target();
	if (form.getIsSigned())
		returned += ", signed";
	returned += ".";
	return (str << form.getName() << returned);
}

std::string				PresidentialPardonForm::get_target(void) const
{
	return (target);
}
