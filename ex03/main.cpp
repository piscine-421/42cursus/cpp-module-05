#include "AForm.hpp"
#include "Intern.hpp"
#include "Bureaucrat.hpp"
#include <iostream>

int main(void)
{
	std::cout << "stuff in PDF" << std::endl;
	Intern	someRandomIntern;
	AForm	*rrf;

	rrf = someRandomIntern.makeForm("robotomy request", "Bender");
	std::cout << std::endl << "Maor:" << std::endl;
	if (rrf)
		delete rrf;
	AForm	*ppf;
	ppf = someRandomIntern.makeForm("presidential pardon", "Hunter Biden");
	if (ppf)
		delete ppf;
	AForm	*scf;
	scf = someRandomIntern.makeForm("shrubbery creation", "some forest somewhere in great britain or wherever that scene in monty python took place idk");
	if (scf)
		delete scf;
	AForm	*invalid;
	invalid = someRandomIntern.makeForm("how should I know", "we haven't had any targets in canada since 2015, sorry");
	if (invalid)
		delete invalid;
}