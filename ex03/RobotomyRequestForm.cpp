#include "AForm.hpp"
#include "Bureaucrat.hpp"
#include "RobotomyRequestForm.hpp"

AForm					*RobotomyRequestForm::makeForm(AForm *form, std::string name, std::string target)
{
	if (!form && name == "robotomy request")
		return (new RobotomyRequestForm(target));
	return (form);
}

RobotomyRequestForm::~RobotomyRequestForm(void)
{
}

RobotomyRequestForm::RobotomyRequestForm(void): AForm::AForm(45, 72, "RobotomyRequestForm"), target("null")
{
}

RobotomyRequestForm::RobotomyRequestForm(const std::string &target_file): AForm::AForm(45, 72, "RobotomyRequestForm"), target(target_file)
{
}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &copy): AForm::AForm(copy), target(copy.target)
{
}

RobotomyRequestForm 	&RobotomyRequestForm::operator=(const RobotomyRequestForm &copy)
{
	AForm::operator=(copy);
	this->target = copy.target;
	return (*this);
}

void					RobotomyRequestForm::execute_form(void) const
{
	static bool	seed_set = false;

	if (seed_set == false)
	{
		srand(time(NULL));
		seed_set = true;
	}
	std::cout << "DRRRRRRRRRRR!" << std::endl;
	if (std::rand() % 2)
		std::cout << target << " robotomized successfully." << std::endl;
	else
		std::cout << target << " could not be robotomized." << std::endl;
}

std::ostream			&operator<<(std::ostream &str, const RobotomyRequestForm &form)
{
	std::string	returned;

	returned = ", grade to execute " + std::to_string(form.getGradeToExecute()) + ", grade to sign " + std::to_string(form.getGradeToSign()) + ", target " + form.get_target();
	if (form.getIsSigned())
		returned += ", signed";
	returned += ".";
	return (str << form.getName() << returned);
}

std::string				RobotomyRequestForm::get_target(void) const
{
	return (target);
}
