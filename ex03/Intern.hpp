#ifndef INTERN_HPP
# define INTERN_HPP
# include <string>

class	AForm;

class	Intern
{
	private:
	public:
		AForm	*makeForm(std::string name, std::string target);
		~Intern(void);
		Intern(void);
		Intern(const Intern &copy);
		Intern	&operator=(const Intern &copy);
};

#endif
