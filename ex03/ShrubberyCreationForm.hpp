#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP
# include "AForm.hpp"

class			Bureaucrat;

class			ShrubberyCreationForm: public AForm
{
	private:
		std::string	target;
	public:
		static AForm			*makeForm(AForm *form, std::string name, std::string target);
		void					execute_form(void) const;
		std::string				get_target(void) const;
		~ShrubberyCreationForm(void);
		ShrubberyCreationForm(void);
		ShrubberyCreationForm(const std::string &target_file);
		ShrubberyCreationForm(const ShrubberyCreationForm &copy);
		ShrubberyCreationForm	&operator=(const ShrubberyCreationForm &copy);
};

std::ostream	&operator<<(std::ostream &str, const ShrubberyCreationForm &form);

#endif
