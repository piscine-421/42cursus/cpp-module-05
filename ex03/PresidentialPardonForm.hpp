#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP
# include "AForm.hpp"

class			Bureaucrat;

class			PresidentialPardonForm: public AForm
{
	private:
		std::string	target;
	public:
		static AForm			*makeForm(AForm *form, std::string name, std::string target);
		void					execute_form(void) const;
		std::string				get_target(void) const;
		~PresidentialPardonForm(void);
		PresidentialPardonForm(void);
		PresidentialPardonForm(const std::string &target_file);
		PresidentialPardonForm(const PresidentialPardonForm &copy);
		PresidentialPardonForm	&operator=(const PresidentialPardonForm &copy);
};

std::ostream	&operator<<(std::ostream &str, const PresidentialPardonForm &form);

#endif
